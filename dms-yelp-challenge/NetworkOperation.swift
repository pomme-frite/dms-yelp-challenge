import Foundation

class NetworkOperation<T, E>: Operation where E: Error {
    enum OperationState: String {
        case none
        case ready = "isReady"
        case executing = "isExecuting"
        case finished = "isFinished"
    }
    
    var operationState: OperationState = .none {
        willSet {
            willChangeValue(forKey: newValue.rawValue)
        }
        
        didSet {
            didChangeValue(forKey: operationState.rawValue)
        }
    }
    
    private let request: URLRequest
    private let decode: (Data?, URLResponse?, Error?) -> Result<T, E>
    private let completion: ((Result<T, E>) -> Void)?
    
    init(request: URLRequest,
         decode: @escaping (Data?, URLResponse?, Error?) -> Result<T, E>,
         completion: ((Result<T, E>) -> Void)? = nil) {
        self.request = request
        self.decode = decode
        self.completion = completion
        operationState = .ready
    }
    
    override func start() {
        if !isExecuting {
            operationState = .executing
            performRequest()
        }
    }
    
    func performRequest() {
        let task: URLSessionDataTask =
            URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
                guard let self: NetworkOperation<T, E> = self else { return }
            
            let result: Result<T, E> = self.decode(data, response, error)
            
            switch result {
            case .success(let value):
                DispatchQueue.main.async {
                    self.completion?(Result.success(value))
                    self.operationState = .finished
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self.completion?(Result.failure(error))
                    self.operationState = .finished
                }
            }
        }
        
        task.resume()
    }
    
    
    // MARK: operation states
    
    override var isReady: Bool {
        return operationState == .ready
    }
    
    override var isExecuting: Bool {
        return operationState == .executing
    }
    
    override var isFinished: Bool {
        return operationState == .finished
    }
    
    override var isAsynchronous: Bool {
        return true
    }
}
