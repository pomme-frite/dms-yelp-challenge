import Foundation

class NetworkService {
    let queue: OperationQueue = OperationQueue()
    
    init() {
        queue.maxConcurrentOperationCount = 1
        queue.qualityOfService = .userInteractive
    }
    
    func add(operation: Operation) {
        queue.addOperation(operation)
    }
    
    func cancelAll() {
        queue.cancelAllOperations()
    }
    
    func cancelLast() {
        queue.operations.last?.cancel()
    }
}
