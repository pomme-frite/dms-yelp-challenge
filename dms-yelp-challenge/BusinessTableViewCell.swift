import UIKit

protocol StringIdentifiable {
    static var identifier: String { get }
}

extension StringIdentifiable {
    static var identifier: String { return String(describing: self) }
}

class BusinessTableViewCell: UITableViewCell, StringIdentifiable {
    var distance: String? {
        get { return distanceLabel.text }
        set { distanceLabel.text = newValue }
    }
    var title: String? {
        get { return textLabel?.text }
        set { textLabel?.text = newValue }
    }
    var rating: Double? {
        didSet {
            ratingLabel.text = "\(String(describing: (rating ?? 0.0))) / 5"
        }
    }
    private let distanceLabel: UILabel = UILabel()
    private let ratingLabel: UILabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        contentView.addSubview(distanceLabel)
        contentView.addSubview(ratingLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let insets: UIEdgeInsets = UIEdgeInsets(top: 15.0, left: 15.0, bottom: 15.0, right: 15.0)
        
        if let titleLabel: UILabel = textLabel {
            let fittingSize: CGSize = CGSize(width: bounds.width - insets.left - insets.right,
                                             height: bounds.height - insets.top - insets.bottom)
            titleLabel.bounds.size = titleLabel.sizeThatFits(fittingSize)
            titleLabel.center = CGPoint(x: insets.left + titleLabel.bounds.midX,
                                        y: insets.top + titleLabel.bounds.midY)
        }
        
        ratingLabel.sizeToFit()
        ratingLabel.center = CGPoint(x: insets.left + ratingLabel.bounds.midX,
                                       y: bounds.height - insets.bottom - ratingLabel.bounds.midY)
        
        distanceLabel.sizeToFit()
        distanceLabel.center = CGPoint(x: bounds.width - insets.right - distanceLabel.bounds.midX,
                                     y: bounds.height - insets.bottom - distanceLabel.bounds.midY)
    }
}
