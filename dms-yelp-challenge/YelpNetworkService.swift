import Foundation

class YelpNetworkService: NetworkService {
    static let host: String = "https://api.yelp.com/v3/"
    static let shared: YelpNetworkService = YelpNetworkService()
    
    static func urlRequest(withPath path: String, parameters: [URLQueryItem]) -> URLRequest {
        var urlComponents: URLComponents = URLComponents(string: host + path)!
        urlComponents.queryItems = parameters
        var request: URLRequest = URLRequest(url: urlComponents.url!)
        let apiKey: String = "aOGNhjPE2EIN5NIndaHD6TGs34QnRy5RvQnxnlblyuAGr2HG-DqJsLVTh2ASpFb0m9jIADUDD_3w_Uv6zRrC2gHpb8ZXraSr4d1B2AMJO6fywxZGwCDB8fkxmaHMW3Yx"
        request.addValue("Bearer \(apiKey)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        return request
    }
}
