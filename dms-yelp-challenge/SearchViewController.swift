import UIKit

protocol SearchViewControllerDelegate: class {
    func searchViewControllerDidFind(items: [Item])
    func searchViewControllerDidSelect(item: Item)
}

class SearchViewController: UIViewController, SearchBarViewControllerDelegate, BusinessesTableViewControllerDelegate {
    weak var delegate: SearchViewControllerDelegate?
    
    private let searchBarVC: SearchBarViewController = SearchBarViewController()
    private let resultsVC: BusinessesTableViewController = BusinessesTableViewController()
    
    override func resignFirstResponder() -> Bool {
        if searchBarVC.isFirstResponder {
            return searchBarVC.resignFirstResponder()
        } else {
            return true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBarVC.delegate = self
        addChildWithView(searchBarVC)
        addChildWithView(resultsVC)
        
        resultsVC.delegate = self
        
        view.round(corners: [.topLeft, .topRight], radius: 8.0)
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor(red: 194, green: 194, blue: 199, alpha: 1.0)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        preferredContentSize = CGSize(width: UIScreen.main.bounds.width,
                                      height: UIScreen.main.bounds.height * 0.4)
        
        searchBarVC.view.setNeedsLayout()
        searchBarVC.view.layoutIfNeeded()
        searchBarVC.view.bounds.size = searchBarVC.preferredContentSize
        searchBarVC.view.center = CGPoint(x: view.bounds.midX, y: searchBarVC.view.bounds.midY)
        
        resultsVC.view.bounds.size = CGSize(width: view.bounds.width,
                                            height: searchBarVC.view.frame.maxY - preferredContentSize.height)
        resultsVC.view.center = CGPoint(x: view.bounds.midX,
                                        y: searchBarVC.view.frame.maxY + resultsVC.view.bounds.midY)
    }
    
    func select(business: Business) {
        resultsVC.select(business: business)
    }
    
    func deselect(business: Business) {
        resultsVC.deselect(business: business)
    }
    
    // MARK: SearchBarViewControllerDelegate
    
    func searchBarViewControllerDidFind(items: [Item]) {
        resultsVC.items = items.compactMap { $0 as? Business }
        delegate?.searchViewControllerDidFind(items: items)
    }
    
    // MARK: BusinessesTableViewControllerDelegate
    
    func businessesViewControllerDidSelect(business: Business) {
        delegate?.searchViewControllerDidSelect(item: business)
    }
}
