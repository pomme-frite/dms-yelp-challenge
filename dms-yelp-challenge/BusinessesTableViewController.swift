import UIKit
import MapKit

protocol BusinessesTableViewControllerDelegate: class {
    func businessesViewControllerDidSelect(business: Business)
}

class BusinessesTableViewController: UITableViewController {
    var items: [Business] = [] {
        didSet {
            tableView.reloadSections(IndexSet(arrayLiteral: 0), with: .automatic)
        }
    }
    weak var delegate: BusinessesTableViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(BusinessTableViewCell.self, forCellReuseIdentifier: BusinessTableViewCell.identifier)
        tableView.rowHeight = 80.0
    }
    
    func select(business: Business) {
        guard let index: Int = items.firstIndex(where: { item in item.id == business.id }) else { return }
        tableView.selectRow(at: IndexPath(item: index, section: 0), animated: true, scrollPosition: .middle)
    }
    
    func deselect(business: Business) {
        guard let index: Int = items.firstIndex(where: { item in item.id == business.id }) else { return }
        tableView.deselectRow(at: IndexPath(item: index, section: 0), animated: true)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item: Business = items[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: BusinessTableViewCell.self), for: indexPath) as! BusinessTableViewCell
        cell.title = item.name
        cell.distance = MKDistanceFormatter().string(fromDistance: item.distance)
        cell.rating = item.rating
        
        return cell
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let business: Business = items[indexPath.row]
        delegate?.businessesViewControllerDidSelect(business: business)
    }
}
