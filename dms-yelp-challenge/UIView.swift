import UIKit

extension UIView {
    var safeAreaInsetsIfAvailable: UIEdgeInsets {
        if #available(iOS 11.0, *) {
            return safeAreaInsets
        } else {
            return .zero
        }
    }
    
    var geometricCenter: CGPoint { return CGPoint(x: bounds.midX, y: bounds.midY) }
    
    func round(corners: UIRectCorner, radius: CGFloat) {
        layer.cornerRadius = radius
        layer.maskedCorners = corners.caCornerMask
    }
}

extension UIRectCorner {
    var caCornerMask: CACornerMask {
        var mask: CACornerMask = CACornerMask()
        
        if self.contains(UIRectCorner.allCorners) {
            mask = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        } else {
            if self.contains(UIRectCorner.topLeft) {
                mask.insert(.layerMinXMinYCorner)
            }
            
            if self.contains(UIRectCorner.topRight) {
                mask.insert(.layerMaxXMinYCorner)
            }
            
            if self.contains(UIRectCorner.bottomLeft) {
                mask.insert(.layerMinXMaxYCorner)
            }
            
            if self.contains(UIRectCorner.bottomRight) {
                mask.insert(.layerMaxXMaxYCorner)
            }
        }
        
        return mask
    }
}

