import Foundation

protocol EntityDecoder {
    associatedtype T
    func decode(data: Data) -> Result<T, Error>
}
