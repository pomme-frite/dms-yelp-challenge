import Foundation
import CoreLocation

struct Business: Decodable, Item {
    let id: String
    let name: String
    let coordinate: CLLocationCoordinate2D
    let distance: Double
    let rating: Double
    
    enum CodingKeys: String, CodingKey {
        case id
        case coordinate = "coordinates"
        case name
        case distance
        case rating
    }
}

struct BusinessNetworkOperation {
    enum Error: Swift.Error {
        
    }
    
    static func fetchClosestBusinesses(fromLocation location: CLLocation, categories: [String], forQuery query: String,
                                 completion: @escaping (Result<[Business], YelpNetworkOperationError>) -> Void) -> YelpNetworkOperation<[Business]> {
        
        let categoriesString: String = categories.joined(separator: ",")
        
        let path: String = "businesses/search"
        let parameters: [URLQueryItem] = [
            URLQueryItem(name: "term", value: query),
            URLQueryItem(name: "categories", value: categoriesString),
            URLQueryItem(name: "sort_by", value: "distance"),
            URLQueryItem(name: "latitude", value: String(location.coordinate.latitude)),
            URLQueryItem(name: "longitude", value: String(location.coordinate.longitude))
        ]
        let request: URLRequest = YelpNetworkService.urlRequest(withPath: path, parameters: parameters)
        return YelpNetworkOperation(request: request, completion: { result in
            switch result {
            case .success(let value):
                completion(.success(value))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
}
