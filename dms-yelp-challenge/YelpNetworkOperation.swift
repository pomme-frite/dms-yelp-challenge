import Foundation

enum YelpNetworkOperationError: Error {
    case decoding
    case unknown
}

class YelpNetworkOperation<T>: NetworkOperation<T, YelpNetworkOperationError> where T: Decodable {
    
    init(request: URLRequest, completion: ((Result<T, YelpNetworkOperationError>) -> Void)?) {
        super.init(request: request,
                   decode: YelpNetworkOperation.decode,
                   completion: completion)
    }
    
    private static func decode(data: Data?, response: URLResponse?, error: Error?) -> Result<T, YelpNetworkOperationError> {
        guard let data: Data = data else {
            return Result.failure(.decoding)
        }
        
        let decoder: YelpDecoder<T> = YelpDecoder<T>()
        let result = decoder.decode(data: data)
        
        switch result {
        case .success(let response):
            if let value: T = response.businesses {
                return Result.success(value)
            } else {
                return Result.failure(.unknown)
            }
        case .failure(let error):
            print(error)
            return Result.failure(.decoding)
        }
    }
}
