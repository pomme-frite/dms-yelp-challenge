import UIKit

extension UIViewController {
    func addChild(_ child: UIViewController, to parentView: UIView) {
        self.addChild(child)
        parentView.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    func addChildWithView(_ child: UIViewController) {
        addChild(child, to: view)
    }
}
