import UIKit
import CoreLocation

protocol AnnotationModel {
    var coordinate: CLLocationCoordinate2D { get }
    var id: String { get }
}

func == (lhs: AnnotationModel, rhs: AnnotationModel) -> Bool {
    return type(of: lhs) == type(of: rhs) && lhs.id == rhs.id
}
