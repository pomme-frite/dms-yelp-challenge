import Foundation

struct YelpDecoder<X>: EntityDecoder where X: Decodable {
    struct Response<D>: Decodable where D: Decodable {
        let businesses: D?
    }
    
    func decode(data: Data) -> Result<Response<X>, Error> {
        do {
            let decoded: YelpDecoder<X>.Response<X> = try JSONDecoder().decode(Response<X>.self, from: data)
            return Result.success(decoded)
        } catch {
            return Result.failure(error)
        }
    }
}
