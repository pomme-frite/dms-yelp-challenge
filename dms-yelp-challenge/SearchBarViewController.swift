import UIKit
import CoreLocation

protocol Item: AnnotationModel {
    var name: String { get }
}

protocol SearchBarViewControllerDelegate: class {
    func searchBarViewControllerDidFind(items: [Item])
}

class SearchBarViewController: UIViewController, UISearchBarDelegate {
    private let searchBar: UISearchBar = UISearchBar()
    weak var delegate: SearchBarViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .darkGray
        view.addSubview(searchBar)
        
        searchBar.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        searchBar.sizeToFit()
        searchBar.center = CGPoint(x: searchBar.bounds.midX, y: searchBar.bounds.midY)
        preferredContentSize = CGSize(width: UIScreen.main.bounds.width, height: searchBar.bounds.height)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        searchBar.becomeFirstResponder()
    }
    
    override var isFirstResponder: Bool {
        return searchBar.isFirstResponder
    }
    
    override func resignFirstResponder() -> Bool {
        super.resignFirstResponder()
        return searchBar.resignFirstResponder()
    }
    
    // MARK: UISearchBarDelegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let location: CLLocation = CLLocationManager().location else { return }
        
        YelpNetworkService.shared.cancelAll()
        
        let op: YelpNetworkOperation = BusinessNetworkOperation
            .fetchClosestBusinesses(fromLocation: location,
                                    categories: ["restaurant"],
                                    forQuery: searchText,
                                    completion: { [weak self] result in
                                        guard let self: SearchBarViewController = self else { return }
                                        switch result {
                                        case .success(let restaurants):
                                            self.delegate?.searchBarViewControllerDidFind(items: restaurants)
                                        case .failure(let error):
                                            print(error)
                                        }
                                        
            })
        YelpNetworkService.shared.add(operation: op)
    }
}
